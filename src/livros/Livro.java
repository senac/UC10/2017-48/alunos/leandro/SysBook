package livros;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala302b
 */
public class Livro {

    private String titulo;
    private int isbn;
    private int anoEdicao;
    private String editora;
    
    private List livro = new ArrayList();

    public Livro(String titulo, int isbn, int anoEdicao, String editora) {
        this.titulo = titulo;
        this.isbn = isbn;
        this.anoEdicao = anoEdicao;
        this.editora = editora;
        
    }

    public Livro() {
    }

    
    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public int getAnoEdicao() {
        return anoEdicao;
    }

    public void setAnoEdicao(int anoEdicao) {
        this.anoEdicao = anoEdicao;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

  

    
    
}
