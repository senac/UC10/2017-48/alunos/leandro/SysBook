/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emprestimo;

import java.util.Date;

/**
 *
 * @author sala302b
 */
public class Emprestimo {
    
    private Date dtEntrega;
    private Date dtDevolucao;

    public Date getDtEntrega() {
        return dtEntrega;
    }

    public void setDtEntrega(Date dtEntrega) {
        this.dtEntrega = dtEntrega;
    }

    public Date getDtDevolucao() {
        return dtDevolucao;
    }

    public void setDtDevolucao(Date dtDevolucao) {
        this.dtDevolucao = dtDevolucao;
    }

    public Emprestimo(Date dtEntrega, Date dtDevolucao) {
        this.dtEntrega = dtEntrega;
        this.dtDevolucao = dtDevolucao;
    }
    
    
    
    
}
