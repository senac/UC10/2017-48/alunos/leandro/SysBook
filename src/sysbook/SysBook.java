/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysbook;

import cliente.Cliente;
import telas.JFramePrincipal;
import java.util.*;
import emprestimo.Emprestimo;
import genero.Genero;
import livros.Autor;
import livros.Livro;


public class SysBook {
    
    public static List <Cliente> listCliente = new ArrayList<>();
    public static List <Emprestimo> listEmprestimo= new ArrayList<>();
    public static List <Genero> listGenero = new ArrayList<>();
    public static List <Autor> listAutor = new ArrayList<>();
    public static List <Livro> listLivro = new ArrayList<>();

    public static List<Cliente> getListCliente() {
        return listCliente;
    }

    public static void setListCliente(List<Cliente> listCliente) {
        SysBook.listCliente = listCliente;
    }

    public static List<Emprestimo> getListEmprestimo() {
        return listEmprestimo;
    }

    public static void setListEmprestimo(List<Emprestimo> listEmprestimo) {
        SysBook.listEmprestimo = listEmprestimo;
    }

    public static List<Genero> getListGenero() {
        return listGenero;
    }

    public static void setListGenero(List<Genero> listGenero) {
        SysBook.listGenero = listGenero;
    }

    public static List<Autor> getListAutor() {
        return listAutor;
    }

    public static void setListAutor(List<Autor> listAutor) {
        SysBook.listAutor = listAutor;
    }

    public static List<Livro> getListLivro() {
        return listLivro;
    }

    public static void setListLivro(List<Livro> listLivro) {
        SysBook.listLivro = listLivro;
    }
    
    
    
    
    
    public static void main(String[] args) {
        
        new JFramePrincipal();
        
    }
    
    public static void addCli (Cliente cliente){
        listCliente.add(cliente);
    }
    
    public static void addGenero (Genero genero){
        listGenero.add(genero);
    }
    
    public static void addAutor (Autor autor){
        listAutor.add(autor);
    }
    
    public static void addLivro (Livro livro){
        listLivro.add(livro);
    }
 
    public static void addEmprestimo (Emprestimo emprestimo){
        listEmprestimo.add(emprestimo);
    }
}
